﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(ELIFOTEL.Startup))]
namespace ELIFOTEL
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
