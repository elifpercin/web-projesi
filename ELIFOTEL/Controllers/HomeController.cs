﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ELIFOTEL.Models;
using System.Net;

namespace ELIFOTEL.Controllers
{
    public class HomeController : Controller
    {
        OtelEntities db = new OtelEntities();
        public ActionResult Create(Rezerve model)
        {
            try
            {
                model.RId = 1;
                db.Rezerve.Add(model);
                db.SaveChanges();
                return RedirectToAction("KayitBasarili");
            }
            catch (Exception)
            {

                throw;
            }
        }
        public ActionResult Musteri()
        {
            return View();

        }


        public ActionResult Uye()
        {
            return View();

        }

        [HttpPost]
        public ActionResult Uye(FormCollection form)
        {
            Uye model1 = new Uye();

            try
            {

                model1.UyeAd = form["UyeAd"].Trim();
                model1.UyeSoyad = form["UyeSoyad"].Trim();
                model1.UyeMail = form["UyeMail"].Trim();
                model1.UyeSifre = form["UyeSifre"].Trim();
                db.Uye.Add(model1);
                db.SaveChanges();

                return RedirectToAction("KayitBasarili");
            }
            catch (Exception)
            {

                throw;
            }

            

        }

        public ActionResult KayitBasarili()
        {
            return View();
        }


        public ActionResult Index()
        {
            return View();
        }
        public ActionResult Felsefe()
        {
          
            return View();
        }
        public ActionResult Fiyat()
        {

            return View();
        }
        public ActionResult Galeri()
        {

            return View();
        }
        public ActionResult Hizmet()
        {

            return View();
        }
        public ActionResult Iletisim()
        {

            return View();
        }
        public ActionResult Odalar()
        {

            return View();
        }
        public ActionResult UyeGiris()
        {

            return View();
        }


        public ActionResult Rezerve()
        {

            return View();
        }


        public ActionResult Giris(string email, string sifre)
        {
            //Gelen Email ve şifre bilgisini veri tabanında aradık ve gelen ilk kaydı aldık.
            var GirisYapan = db.Uye.Where(a => a.UyeMail == email && a.UyeSifre == sifre).FirstOrDefault();
            //Eğer bilgi varsa Shared altındaki basarili view’ine git.
            if (GirisYapan != null)
            {
                return View("Felsefe");
            }
            else
                //Eğer bilgiler uyuşmazsa ana sayfada kayıları göster.
                //return View("Giris",db.Uye);

                return RedirectToAction("KayitBasarili");
        }

    }
}